package watch

import (
	"fmt"
	"testing"
	"time"
)

func TestWatch(t *testing.T) {
	f := func() { fmt.Println("this is THE function") }
	ch1 := make(chan interface{})
	ch2 := make(chan interface{})

	go Watch(f, ch1, ch2)

	ch1 <- nil
	ch1 <- nil
	ch1 <- nil
	close(ch1)
	ch2 <- nil
	close(ch2)
	time.Sleep(time.Millisecond)
}
