package watch

import (
	"sync"
)

// Watch listens to channels and calls f when something is received. Usage: go Watch(...)
func Watch(f func(), cs ...<-chan interface{}) {
	eventC := make(chan interface{})
	var wg sync.WaitGroup
	wg.Add(len(cs))
	for _, c := range cs { // Note: c is a SINGLE variable. COPY it to each goroutine before it changes value!
		go func(d <-chan interface{}) {
			for range d {
				eventC <- nil
			}
			wg.Done()
		}(c)
	}
	go func() {
		wg.Wait()
		close(eventC)
	}()
	for range eventC {
		f()
	}
}
